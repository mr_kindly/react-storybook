import React from 'react';
import classnames from 'classnames';


const Price = ({ price }) => {
  const { value, discount, currency } = price;
  const newValue = (value * (100 - discount)) / 100;
  const priceStyle = classnames({
    'text-danger': !!discount,
  });
  const crossedStyle = {
    textDecoration: 'line-through',
  };

  return (
        <h4>
            <span className={priceStyle} style={crossedStyle}>
                {value}{currency}
            </span>
            {!!discount && <span className="text-success"> {newValue}{currency} </span>}
        </h4>);
};

export default Price;
