import React from 'react';

import { storiesOf } from '@storybook/react';
import { withInfo } from "@storybook/addon-info";

import Price from './../src/components/price';

const priceWithDiscount = {
  value: 200,
  discount: 10,
  currency: '$',
};

const priceWithoutDiscount = {
  value: 200,
  currency: '$',
};

const Red = props => <span style={{ color: "red" }} {...props} />;

const TableComponent = ({ propDefinitions }) => {
  const props = propDefinitions.map(
    ({ property, propType, required, description, defaultValue }) => {
      return (
        <tr key={property}>
          <td>
            {property}
            {required ? <Red>*</Red> : null}
          </td>
          <td>{propType.name}</td>
          <td>{defaultValue}</td>
          <td>{description}</td>
        </tr>
      );
    },
  );

  return (
    <table>
      <thead>
        <tr>
          <th>name</th>
          <th>type</th>
          <th>default</th>
          <th>description</th>
        </tr>
      </thead>
      <tbody>{props}</tbody>
    </table>
  );
};

storiesOf('Price', module)
  .add('with discount', () => <Price price={priceWithDiscount} />)
  .add('without discount', () => <Price price={priceWithoutDiscount} />)
  .add('with info', withInfo({})(() => <Price price={priceWithoutDiscount} />));
